from django.urls import path
from receipts.views import receipt, create_receipt

urlpatterns = [
    path("", receipt, name="receipt_list"),
    path("create/", create_receipt, name="create_receipt")
        #addressbar, function name, redirect name 
]
