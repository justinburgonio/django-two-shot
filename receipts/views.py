from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from receipts.models import Receipt
from receipts.forms import ReceiptForm


# Create your views here.

@login_required
def receipt(request):
    receipt_list = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": receipt_list,
    }
    return render(request, "receipts/list.html", context)

@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            saved = form.save(False)
            # make sure the form isnt saved
            # until we make sure we have a purchaser
            saved.purchaser = request.user
            # setting the purchaser as the user
            saved.save()
            # saved
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {
        "form": form
    }
    return render(request, "receipts/create.html", context)